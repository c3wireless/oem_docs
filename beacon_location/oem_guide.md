% OEM Guide; C3 BLE Location System
% Shawn Nock, Jaycon Systems
% v1.2; 4 Jan. 2016

# System Overview

## Location Concepts

Beacon:
: A combination of identifying (uuid, major, minor) and location
metadata (`geom` field; point) representing a single iBeacon. The
design assumption is that the (uuid, major, minor) will be unique to a
single device.

Listener:
: A combination of identifying (mac) and location metadata (`geom`
field; point) representing a single listener hardware device. A design
assumption for listeners is that their self-reported `mac` field will
be unique to a single listener device and that their geographic
location will be accurately specified in the web interface, admin
panel, or GIS software interface.

ROI:
: Region of Interest. A polygon area on the earths surface that
represents a single logical installation. In a healthcare setting, for
example, each hospital may be it's own ROI. ROI's are used for map
navigation in the web interface, and other geographic info can be
filtered by ROI. It is a design assumption of the system that all
zones, listeners and beacons be located within an accurately located
ROI polygon and rarely move between them.

Zone:
: Zones are intended to break up ROI into smaller logical groups,
i.e. rooms or areas. Zones are also used to constrain error in
low-resolution location scenarios.

## Privilege / Authorization Model

Users are the authentication units. Only users have authentication
credentials (password and/or associated API keys). There are three
types of users:

1. Superusers
    Superusers may modify any object in the system and assign groups
    and privileges in the system
	
2. Admin Users
    Admin users may login to the admin panel, but their actual access
    there in is limited by permissions specified by Superusers/Admin
    users.
	
3. Normal Users
    Normal users may not access the admin panel, and all API results /
    Web views are filtered to only include objects owned by groups of
    which they are members.
	
Users may be assigned to an arbitrary number of *groups*. Groups are
the basic unit of authorization. Users inheirit access to objects from
groups and admin users also inherit privileges in the admin panel
from groups.

## Components

### Beacons

C3 makes a line of beacons broadly compatible with iBeacon™. Some
optional enhancements to iBeacon™ protocol are supported:

 - Battery level advertisement
   
    At programmable intervals, the beacon can transmit the current
    battery life percentage.
   
 - Longer Intervals
   
    For some applications the 100ms max advertisement interval
	specified by iBeacon isn't ideal. C3 Beacons can be programmed to
    advertise at intervals between 20ms and 10000ms so that the OEM may
    choose between increase battery life and increased location
    accuracy.
   
### Listener Hardware

Listeners are custom hardware featuring the following:

 - AR9331 clocked at 400MHz; Embedded Linux OS
 - 802.11bg Wireless Radio
 - 100Mbit/s Ethernet, including 802.3af Power-over-ethernet
 - 2.4GHz Bluetooth 4.0 Radio
 
Listeners monitor the BLE advertisement channels for iBeacon
compatible packets; beacon distance/power is filtered and aggregated
by the listener before being forwarded to the location service at
configurable intervals

The listener packet format is documented in the *packet-format*
document.

### Location Daemon

The location daemon aggregates data from all listeners and assigns
beacons a geographic location using several algorithms:

#### ROI Locking:

When:

 - One Listener providing data
 - Nearest listener(s) is not assigned to a zone
 - Beacon has no current location, or nearest listener changes
 
Beacon is located at the distance reported from the nearest listener
on a line drawn between the nearest listener and the centroid of the
nearest ROI. The design assumption is, that given no other
information, the beacon is more likely to be near the center of an ROI
than at the extremes.
 
![ROI Locked](listener-locked.png){width=60%}

#### Zone Locking

When:

 - One listener providing data
 - Nearest listener(s) assigned to a zone
 - Beacon has no current location, or nearest listener changes
 
Beacon is located at the distance reported from the nearest listener
on a line drawn between the nearest listener and the centroid of
listener's zone. If the distance would place the listener outside of
the zone, then the distance is truncated at the zone border.

The design assumptions are:

#. Zones are often logically analogous to rooms
#. Objects requireing tracking are more likely to be nearer to the
   center of rooms, rather than at the edges (dubious)
#. Placing low-quality location results near the center of a zone
   aides operator awareness.

![Zone Locked](zone-locked.png){width=60%}

#### Multilateration:

When:

 - Two or more listeners are providing data
 - At fixed update interval expires
 
Weighted non-linear least squares error minimization locates the
beacon between the listeners. Weighting factors are distance, number
of packets received in the update interval, and age of the report
data. 

![Multilateration](multilat-nozone.png){width=60%}

If the nearest listener is assigned to a zone, then the location is
truncated at border of the zone (on a line from the listener to the
predicted location) if the location would have been outside the zone
boundary.

![Multilateration, Limited by Zone](multilat-zone.png){width=60%}


#### Additional Filtering (unimplemented; Jan. 2016)

The calculated location is smoothed by a 2D Kalman filter with
variance data provided by the least-squares algorithm or constant
large variance for the zone/listener locked algoritm.

### API Server

The API server provides access to the beacon, listener, zone, and roi
data via REST and WAMP (RPC+pubsub).

#### REST API

`http://core4.nocko.se:8880/` or `http://core4.nocko.se/api/`

The REST API hosts it's own documentation generated from [raml](http://raml.org/) at the root of the API
URL and at [github](http://jayconsystems.github.io/c3api-docs/).

![](api-docs.jpg)\ 

Authentication is meant to be by API keys following the JWT
standard. Currently disabled for ease of testing. Many OEM customers
will only have one API client; and thus authentication may not be
required.

#### WAMP API

WAMP API is currently undocumented and may be deprecated in the
future. Please contact C3 Wireless if this is a feature you'd find
valuable.

### Web UI

### Admin Panel

[`http://core4.nocko.se/admin/`](http://core4.nocko.se/admin/)

Admin and Superusers may login here to add/remove/delete/edit/assign
locations to objects. This is meant to be done (and can be done) via
the API, but this simple application simplifies setup during testing.

![](admin-site.jpg)\ 

### End User UI

[`http://core4.nocko.se/`](http://core4.nocko.se/)

The main user interface is a very basic demo showing what objects are
available to normal users (based on their permissions) and a generic
presentation of location onto a map.

![](user-site.jpg)\ 

ROIs are outlined with *blue dashed lines*, listeners are *red
pentagons* and beacons are *blue circles* 

# Getting Started

## Listener Setup

### Listener Power

Listeners may be powered via 802.3af PoE (Power-over-ethernet) via the
wired ethernet connector *or* via a 5V power supply (center positive)
at the barrel jack. 

### Connecting to Listeners
Windows prerequisites: [WinSCP](http://www.winscp.net) & [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/)

The listeners are configured via their wired networking port. This
port maintains the static address `192.168.7.1` (even when the port is
assigned another address for normal operation *or* DHCP).

The CLI may be accessed as follows

Linux / Mac:
:    `ssh root@192.168.7.1`
   
Windows:
:    Start->Run `cmd`; 
	     
	 `C:\Program Files\PuTTY\putty.exe -ssh root@192.168.7.1`

The default password is: `c3letmein`

![](cli.jpg)\ 

### Firmware Upgrade

1. Connect listener to computer via wired port.
2. Statically assign IP address to computer

	Linux:
	:    `ip addr add 192.168.7.2/24 dev eth0`

	Windows:
	:    [Howto Link](http://www.howtogeek.com/howto/19249/how-to-assign-a-static-ip-address-in-xp-vista-or-windows-7/?PageSpeed=noscript)

	MacOS:
	:    [Howto Link](http://www.howtogeek.com/howto/22161/how-to-set-up-a-static-ip-in-mac-os-x/?PageSpeed=noscript)

3. Copy firmware to Listener
   
    Linux / Mac:
    :    `scp *jffs2-sysupgrade.bin root@192.168.7.1:/tmp`
   
    Windows:
    :    [Upload with WinSCP](https://winscp.net/eng/docs/task_upload) (remote directory should be /tmp)
4. Connect to Listener CLI (see above)

5. Start upgrade procedure, by typing the following at the prompt:

    `sysupgrade -n /tmp/*jffs2-sysupgrade.bin`
	
![](firmware-upgrade.jpg)\ 

The firmware will be flashed and the listener will reboot. Changes to
the network settings will be lost after the upgrade (static IP for
wired *or* custom wireless ssid/key). Contact *C3 Wireless* for
integration of custom settings into a personalized firmware image.

### Listener Settings

The characteristics of the listeners may be changed in a number of
ways by loging into the listener and editing the
`/etc/c3listener.conf` file:

`server` (string; IP address or hostname; default = ""):
: The hostname of the remote server

`port` (string; default = "9999"):
: The remote port number on the remote server

`path_loss` (double; default = 4) :
: The RSSI model used for determining beacon distance needs a path
loss component to summarize the RF characteristics of the local
environment. In clear, outdoor environments this is often below 2.0,
indoors in office environments it is sometimes over 4.0.

`user` (string; default = "nobody"):
: The listening daemon drops privileges after setting up bluetooth
sockets. The daemon will switch to the permissions of this
user.

`report_interval` (integer; milliseconds, default = 500):
: The listening daemon prepares a summary of beacon traffic and sends
it to the server at this interval . No report is sent if no traffic is
observed during the interval

`keep_alive` (integer; seconds, default = 30):
: The listener will transmit a keep alive packet to the server at this
interval so it may distinguish between listener failure
and simply no beacons near the listener.

`haab` (double, meters; default = 0.0):
: *Height above average beacon*

`antenna_correction` (integer, dBm; default = 0.0)
: Antenna gain correction. If a high gain antenna is used, signal
levels reported for beacons will be higher than the advertised
dBm@1m. This value is added to RSSI before distance is
calculated. High gain antennas will require a positive value,
electrically compromised antenna will require negative values.

### Listener Tuning (Optional)

#### Listener Positioning

The listener antenna has a null looking directly into the point
of the antenna and maximum gain between $\ang{40}$--$\ang{80}$ off
axis.

Avoid placing the listener such that most common expected beacon paths
will dwell directly underneath it. 

The ideal height for maximum gain is:

\begin{tikzpicture}[scale=0.25]
    \tkzInit[xmax=5,ymax=5]
	\tkzDefPoint(0,5){C}
	\tkzDefPoint(0,0){B}
	\tkzDefPoint(5,0){A}
	\tkzDefPoint(5,5){E}
	\tkzDefShiftPoint[C](-10:9){A'}
	\tkzDefShiftPoint[C](-80:9){A''}
	\tkzLabelPoint[above](C){Listener}
	\tkzMarkRightAngle(C,B,A)
	\tkzDefMidPoint(B,C) \tkzGetPoint{M}
    \tkzInterLL(C,A')(B,A) \tkzGetPoint{D}
	\tkzDefMidPoint(C,D) \tkzGetPoint{M'}
	\tkzInterLL(C,A'')(M,M') \tkzGetPoint{D'}
    \tkzInterLL(C,A')(M,M') \tkzGetPoint{D''}
    \tkzDrawSegments(B,M M,C C,D M,M')
	%\tkzFillPolygon[fill=yellow](D',D'',C)
	\tkzDrawPoints(M)
	\tkzLabelSegment[left](B,M){$h_{avg.\ beacon}$}
	\tkzLabelSegment[left](C,M){$x$}
	\tkzLabelSegment[below](M,M'){coverage\ distance}
	
\end{tikzpicture}

$$\tan\ang{80} \approx 5.7 = \frac{\mbox{coverage distance}}x$$
$$x = \frac{\mbox{coverage distance}}{5.7}$$
$$\mbox{ideal height} = x + h_{\mbox{avg. beacon}} =
\frac{\mbox{coverage distance}}{5.7} + h_{\mbox{avg. beacon}}$$

For example a listener expected to cover a $\SI{8}{\meter}$ radius, the
ideal height assuming an mean expected beacon height of
$\SI{1.5}{\meter}$ above the floor is: $\frac{\SI{8}{\meter}}{5.7} +
\SI{1.5}{\m} \approx \SI{2.9}{\meter}$ above the floor.

What does it mean, *ideal height*. In this context, it means that the
largest and most uniform portion of the antenna gain is oriented to
receive signals from the largest portion of the wanted coverage area.

In general:

1. Mount the listener on the ceiling
2. Avoid placing the listener directly above expected position of beacons
3. If you have a choice of mounting heights, best location results can
   be found near the ideal height.
4. Larger areas can be covered by placing the listener higher, at the
   cost of widening null zone directly underneath the listener.
   
The equation for x above, represents ideal HAAB (Height above average
beacon). Listeners may be informed of their actual HAAB via this
listener config file. This value may need tuning in the listener
software configuration. See the section on [HAAB](#haab-height-above-average-beacon).

#### Antenna positioning

1. Positioning the antenna perpendicular to and level with the
expected plane of beacons. Most commonly this will be $\ang{90}$ to the floor.

	In general, this means that listeners mounted vertically on a wall
	have straight antennae ($\ang{0}$) and those mounted on ceilings
	/ furniture have fully-bent ($\ang{90}$) antennae.
	
2. Position the antenna pointing toward the plane of expected beacons.

	If the listener is on a wall, mount the unit antennae downward for
    higher installations, antennae upward for lower
    installations. Similarly, if the listener is mounted horizontally
    on top of tall furniture, the antenna should face downward
    (counter-intuitive).

#### HAAB (Height Above Average Beacon)

Positioning listeners above obstacles can improve reception / received
power of beacon transmissions, but will also increases the non-planar
distance the beacon signal must travel (more distance is traveled
vertically rather than horizontally).

Imagine that you are holding a beacon and standing directly underneath
a listener mounted 4 meters above you on the ceiling. The listener
would report that the beacon is 2-3 meters away, but if viewed on a
map, you'd rightfully expect the beacon should be geographically
co-located at the listener.

This can be corrected by use of the
[Pythagorean theorem](https://en.wikipedia.org/wiki/Pythagorean_theorem). 

$$a^2 + b^2 = c^2$$

Where HAAB is one side of a right triangle and RSSI derived distance
is assumed to be the hypotenuse path of the signal; the planar
distance can be calculated

For the above example; 
	let $a = \SI{3}{\meter}$ (HAAB), 
		$b = b$ (unknown planar distance to beacon, in meters), and
		$c = \SI{3}{\meter}$ (signal path distance) 
		
$$3^2 + b^2 = 3^2$$
$$b^2 = 0$$
$$b = \sqrt0 = \SI{0}{\meter}$$

If we move away from the listener and the signal path distance is
reported as 6m; the HAAB correction reduces the planar distance to:

$$3^2 + b^2 = 6^2$$
$$b = \sqrt{27} \approx\SI{5.2}{\meter}$$

In general, other error sources (multipath propagation, dynamic path
loss, lobes in antenna pattern) dominate the total system
error. However, in some environments with high ceilings or small rooms
where beacons are expected to be mostly underneath the listener tuning
this value may be desired.

#### Antenna Gain

Listeners are generally shipped with a dipole with RP-SMA
connector. This unit/antenna combination is the one again which
reference beacon signal strength is calculated. 

If a different antenna is substituted, it will be necessary to modify
the listener `antenna_correction`. High-gain / directional antennas
will require a negative value, electrically compromised antenna will
require positive values in order to produce accurate location data.

The units of `antenna_correction` are dBm. For calibration of unknown
antenna; a beacon may be placed a known distance the listener /
antenna and the `antenna_correction` may be adjusted until the
reported distance is equal to the known distance. Increasing values of
`antenna_correction` result in a decrease in reported distance,
reducing `antenna_correction` results in a larger reported distance.

#### Range Debugging

After [connecting to a listener](#connecting-to-listeners) you may
monitor debug information about received iBeacon packets. At the
command prompt, type `logread -f` and a stream of debugging messages
will be displayed. Those preceeded by ble-udp-bridge will have signal
strength and range info for each received iBeacon packet.

### Listener Networking

By default, listeners use DHCP on their wired Ethernet connection for
the connection details required to reach the location server.

#### Static IP

In order to set a static address for the listener, you'll need to
connect to the listener and edit the `/etc/config/network` file.

1. Connect to listener CLI (as above)
2. `nano /etc/config/network`

At minimum, the `lan2` interface should be changed to look similar to
the `lan` interface. You'll also need to add `option gateway
<gw_ipaddr>`. 

![](static-ip.jpg)\ 

3. Save changes by typing `CTRL-X`.
4. type: `sync; reboot`

Further configuration documentation is available in the
[OpenWRT network documentation](https://wiki.openwrt.org/doc/uci/network)

### Wireless Network

Wireless network access is configured in the `/etc/config/wireless`

1. Connect to CLI (as above)
2. `nano /etc/config/wireless`

The fields of interest are `option ssid`, `option encryption` and
`option key`. By default, connection is attempted to the wireless and
an address is requested via DHCP.

![](wireless-config.jpg)\ 

3. `CTRL-X` to save
4. type: `sync; wifi`
5. Try pinging the location server: `ping core4.nocko.se`
6. If the ping is successful (*$x$ bytes received*), then type `reboot`

Further configuration documentation is available in the
[OpenWRT wireless documentation](https://wiki.openwrt.org/doc/uci/wireless)


### Listener Registration

Once listeners are powered on, and connected to a network with access
to the location server, they will begin appearing in the admin
panel. 

Previously unseen listeners appear without a name or group owner. It's
a good idea to configure the listeners one at a time; the new listener
is then easy to spot in the interface.

1. Login to the admin panel: `http://core4.nocko.se/admin`
2. Select Listeners (under C3API).
3. The new listener(s) will appear as `None: None` in the list view;
   meaning no group assigned = None, and listener name = None.
4. Click on the unnamed listener, and on the resulting form fill-in at
   least:
    - Name
	- Location

To select a location, use the map view. 

![Map View](map-view.jpg){width=60%}

The hand icon ![map view hand](map-view-hand.jpg){height=20px} in the upper
right corner allows for drag-move of the
map. The *plus and minus* icons ![Zoom Control](map-view-zoom.jpg){height=20px} icons control map
zoom. The pencil icon ![](map-view-pencil.jpg){height=20px} lets you place
the listener on the map. When the listener has been placed, it is
represented by an orange circle marker ![](map-view-circle.jpg).
   
Currently the widget for listener placement is quite generic. Future
developments will be to bring ROI jumps to the interface, and provide
optional floorplan / zone overlay. In the interim, more precise
placement is possible by using GIS software to interact directly with
the location database. This is describe in the [*Location Database*](#location-database)
section of this guide.

## Beacon Notes

### Beacon Orientation

The standard beacon has an on board omnidirectional antenna on the
right side (with beacon oriented with the C3 logo facing
you). 

The emissions are polarized in the same plane as the antenna. For best
reception, you should consider mounting the beacon so that the battery
door is parallel to the floor.

![Beacon Antenna](beacon-antenna.jpg){height=0.75in} 

## Create Groups

Groups serve two purposes in the location system: 

1. Admin panel permission grouping.

	You can delegate admin functions to non-superusers. If you need
    several users to share the same set of permissions, you can create
    a group with these admin panel position and add the users to the
    group (instead of selecting the permissions for each user).

2. Object Ownership / Visibility

	Not all users should necessarily see all
    beacons/listeners/zones/roi. All locatable objects in the system
    have an owning group. Only users belonging to the group can
    see/use objects owned by the group.

![Group Form](group-form.jpg)

The Group name is required, and will be prepended to the object names
in the admin panel. In the permissions selector, you may select any
admin panel privileges that this group should inherit. For normal user
groups this will often be empty. Even if permissions are inherited
from a group by a user, unless the user has *Staff status* they will
be unable to login to the admin panel to exercise said permissions.

## Add Users

You can add users via the admin panel. Initially all created users
have no privileges or group memberships.

## Edit User Permissions

After the user is added, you can assign permissions.

![User flags](user-form-type.jpg)

The *Active* flag allow a user to login. To disable all user access,
edit the user and unselect this box.

The *Staff status* flag allow the user to login to the admin panel. It
does not imply any ability to add, change or delete objects; those
specific privileges are managed further down on the form.

The *Superuser status* flag allow all-access to the system by the
user. The user may login to the admin panel, but specific permissions
are not verified.

![Group selection](user-form-groups.jpg)

You may add the user to any number of groups to give access to user
objects and/or admin panel privileges (see
[Create Groups](#create-groups)).

![Permission Selection](user-form-permisisons.jpg)

Permissions (for the admin panel) may be selected and have the same
function as group permissions. Normal users will normally not have any
granted permissions. Even if permissions are assigned, unless the user
has *Staff status* they will be unable to login to the admin panel to
exercise said permissions.

## Manage ROI

ROI are used to filter views of locatable objects. ROI's map well to
larger areas, buildings or facilities. Adding ROI is straight-forward.

![Adding an ROI](roi-form.jpg){height=8cm}

Assign the ROI a name, then select a region on the map. The *zoom* and
*hand* controls work the same as the
[listener map view](#listener-registration). The selection tool
differs in that it's designed to select an area, rather than a single
point.

Each time the map is clicked a point is added to the polygon. The
enclosed area is highlighted to show what would be
contained. Double-clicking finishes the selection.

Finally, you may select an owner for the ROI. An ROI with no owner
will only be available to superusers.

## Zones

Zones are used to constrain location of beacons to likely areas. They
map well to rooms. It's unlikely, for example, that a beacon is
*inside* a wall. Creating a room zone that excludes the no-go areas
ensures that disruptions in location data accuracy don't place beacons
too far off the mark.

Zones are created much the same way as ROI and should also be owned by
a group.

![Zone Creation](zone-form.jpg)

# Location Database

Most commonly, an OEM will want to integrate beacon/listener
registration into their own platform. This will allow their customers
to seamlessly interact with item locations in a familiar way.

Until such interfaces are written. The location database may be edited
directly by use of
[GIS](https://en.wikipedia.org/wiki/Geographic_information_system)
software.

## QGIS

QGIS is no-cost and open-source software that can interact directly
with the location
database. [Downloads](https://www.qgis.org/en/site/forusers/download.html)
are available for Windows, MacOS and Linux.

### Connecting to the Location Database


