% Packet Format - iBeacon
% Shawn Nock - Jaycon Systems
% 17 December 2015

# Report Framing

Since the protocol is based on TCP, framing is required to detect the
start and stop of each report. The framing used is the *Asynchronous
Framing* from the *High-Level Data Link Control (HDLC)*[^hdlc] protocol.

[^hdlc]: [https://en.wikipedia.org/wiki/High-Level_Data_Link_Control#Asynchronous_framing](https://en.wikipedia.org/wiki/High-Level_Data_Link_Control#Asynchronous_framing)

The basic method is using the octet 0x7e as the frame boundary. Each
report will start and end with the byte 0x7e. If 0x7e is present as
data inside the report it is escaped with the octet 0x7d and the fifth
bit is cleared. If the escape character is present in the data it's
escaped with itself and it's fifth bit is cleared. In practice this
can be accomplished simply with a translation table.

Translation Table for framing:

| Data Byte | |Value in frame |
|-----------|-|---------------|
| 0x7e      | → |{0x7d, 0x5e}  |
| 0x7d      | →| {0x7d, 0x5d}  |
| Other     | →| No change     |

Translation Table for un-framing (as done at the authserver):

| Value in frame | |Data Byte |
|-----------|-|---------------|
| {0x7d, 0x5e}      | → | 0x7e |
|{0x7d, 0x5d}      | →|  0x7d  |
| Other     | →| No change     |


The sections that follow assume that the leading and trailing 0x7e
octect has been stripped and any escaped values in between have been
*unescaped* back to their original values.

# Packet Format

````
|---|-----------------|---|
             Data ------^
         ^-- Listener Name (Wifi MAC Addr)
  ^--------- Header (3 bytes)
````

# Header Byte 0x00: Protocol Version & Packet Type

The most significant 4 bits of byte 1 identify the version of the
protocol that the listener speaks.

	 version = byte[0] >> 4;
	 packet_type = (byte[0] & 0x0f);

Current Versions:

 * 0x00: Initial Version

The least significant four bytes identify the packet type.

Current Packet Types:

 * 0x00: Keepalive Packet
 * 0x01: Data Packet
 * 0x02: Security Beacon Packet

See Packet Types section for details.

# Header Byte 0x01: Report Length

For a Keepalive packet this byte may be set to any value, it is
ignored. It has no function, but is reserved for future use.

In an Data (iBeacon) packet, there may be many reports. The report
size is the size (in bytes) of each of report. The number of reports
present is `data.length() / header.report_length` (*this field*).

In a Security Beacon packet, there is only one report in the data
field so this byte is equal to the length of the data field.

# Header Byte 0x02: Listener identifier length

The length of the listener identifier that follows.

    listener_id_len = byte[2]

# Byte 0x03--`(0x03+listener_id_len-1)`: Listener Identifier

The listener identifier is the hostname of the listener. With the
custom hardware, this is set to the MAC address of the wifi interface.

# Byte `(0x03+listener_id_len)`--`EOF`: Packet Data

The data field consists of the remaining bytes of the frame (when
excluding the header and the Listener Identifier).

# Packet Types

## Keepalive Packet
````
|---|-----------------|...|
                        ^-- Data (optional, ignored)
            ^-------------- Listener Name (Wifi MAC Addr)
  ^------------------------ Header (3 bytes)
````

Keepalive packets are sent by the listener if a data report hasn't
been sent for `KEEP_ALIVE_SEC`. It's intended to allow us to
distinguish between no beacon activity and a malfuctioning/missing
listener.

## Data (iBeacon) Packet
````
|---|-----------------|-----|-----|...|-----|
         Beacon Report n ----------------^
         Beacon Report 2 ------^
         Beacon Report 1 ^
       ^ Listener Name (Wifi MAC Addr)
  ^----- Header (3 bytes)
````

Data packets are sent approximately every `REPORT_INTERVAL_MSEC`
interval. The report contains a beacon report section for every beacon
heard during the interval since the previous data packet. Data packets
are not sent if there was no beacon activity during the interval. The
length of each report is given in the header in byte 0x01.

### Beacon Report Format
````
|----------------|--|--|--|--|
         ^--------------------- UUID (16 bytes)
                   ^----------- Major (uint16_t, BE)
                      ^-------- Minor (uint16_t, BE)
                         ^----- Packet Count (uint16_t, BE)
                            ^-- Filtered Distance (uint16_t, BE, in cm)
````

All values are in network byte order (big endian). Packet count is the
count number of packets since the last data packet. Distance is in
centimeters. UUID, major, and minor are from the ibeacon standard.

## Secure Report Format

Within the report the data is structured as follows:

````
|----|----------------|---------|-------|-------|---|--|
   ^---------------- Beacon ID / MAC Addr (uint8_t[6])
             ^------ Nonce (uint8_t[16])
  Enciphered Payload (uint8_t[9]) --^
  MAC Tag (uint8_t[4]) --------------------^
  Filtered Distance (uint16_t, in cm) ------------^
  Distance Variance (m^2*100) -----------------------^
````

All multi-byte values (uint16_t and uint32_t) values are in network
byte order (big endian).

### Beacon Key Derivation

Each beacon is programmed with a 128bit secret *BEACON_KEY* that is
derived from a *MASTER_SECRET* specific to the organization and the
*BEACON_ID* (MAC Address). The Key derivation function (KDF) is
non-reversible and the *MASTER_SECRET* is not stored on the beacon;
thus the compromise of one beacon secret doesn't put other beacons at
risk. The key derivation function is not reversible.

When authenticating beacons for the first time, one must derive the
unique *BEACON_KEY*. The key is derived as follows:
CMAC-AES128(MASTER_KEY, *BEACON_ID*). This means the *BEACON_KEY* is
the result of a CMAC-AES128 round using the organization specific
*MASTER_KEY* as the key and *BEACON_ID* as the message.

### Deciphering and Authenticating the Payload

Once the relevant key is derived or a cache obtained via a datastore
the payload maybe decrypted and authenticated using the EAX mode of the
AES128 block cipher.

EAX mode is implemented in may cryptography libraries, an example and
some test vectors are given in the appendices. The information you'll
need to provide to the EAX implementations are:

- Shared Secret (16 bytes)
- Associated Data (Beacon ID)
- Message (Enciphered Payload)
- Tag (Message Authentication Code [MAC] Tag)

The library will check the *MAC* (which covers the encrypted message
and the Beacon ID). If the *message* is authentic, it will decrypt and
return the plaintext of the *message*

The EAX library used on the beacons is available for reference at
[on GitHub](https://github.com/nocko/nrf51_cryptolib)

### Plaintext Message Contents

Once the message is deciphered, the contents of the nine (9) byte
payload are:

Byte 0x00-0x03
:	Drifting Key (`uint8_t[4]`). See [Drifting Key](#drifting-key)

Byte 0x04-0x07
: Monotonic Clock (`uint32_t`). Advances once per second from initial
power on and is backed up to flash memory on the beacon at intervals.

Byte 0x08
:    Button Flags (`uint8_t`). Button 1 is lsb $\rightarrow$ Button 8 is
msb. (1 = button pressed during since last transmision, 0 = Not
pressed)

# Security Beacon Features

### Message Authentication Code

The packet (including *Beacon ID* as associated data) is protected by
a MAC tag via the EAX mode of AES128. It is computationally infeasible
to generate this tag without access to the shared secret.

### Replay Prevention

The encrypted *monotonic clock* field acts as a counter. The auth
server should track the most recent authenticated monotonic clock in
the datastore and reject any packet with a clock less than or equal to
the last known good clock.

A secure beacon flushes this clock to flash memory at longer intervals
to save power. If a beacon is restarted for some reason (battery
change); the packets it generates for the subsequent interval will be
replays until the monotonic clock advances past the last seen at the
auth server.

### Drifting Key

To detect cloned beacons, the payload contains a *drifting key*. The
beacon updates the key periodically with random information. The
expected uncertainty in the key is a function of the difference
between the last seen *monotonic clock* and the clock in the packet
being verified.

The auth server can use *inconsistant* uncertainty to discern that a
beacon has been tampered with or that two devices with the same shared
secret are operating (cloned device).

The datastore will have no initial reference for the drifting key. It
should be *trusted on first use*.

### Drifting Key Evolution

There are two update intervals `DK0_INTERVAL` (7200s) and
`DK1_Interval` (86400s).

When `monotonic_clock` % `DK0_INTERVAL` == 0:

The dk[3] (the last byte) is shifted left by one bit (but *not*
carried into dk[2]) and a random bit is added on the
right. Pseudocode:

~~~ c
uint8_t dk[4];
...
if (clock % DK0_INTERVAL == 0) {
	dk[3] = (dk[3] << 1) | rnd_bit();
}
~~~

When `monotonic_clock` % `DK1_INTERVAL` $===$ 0:

dk[0]--dk[2] are shifted as a group to the left by one bit and a
random bit is appended on the right. Pseudocode:

~~~ c
uint8_t dk[4];
...
if (clock % DK1_INTERVAL == 0) {
	dk[0] = (dk[0] << 1) | (dk[1] >> 7);
    dk[1] = (dk[1] << 1) | (dk[2] >> 7);
	dk[2] = (dk[2] << 1) | rnd_bit();
}
~~~

# Detecting clones with DK

The auth server *shall* evolve the stored last known good dk
(`last_dk`) forward to approximate the state of the incoming dk
(`new_dk`) by rotating in zeros according to the evolution
algorithm.

Additionally, a mask for the expected uncertainty in the incoming dk
is generated (see below). The masked incoming dk is compared to the
locally evolved dk. Discrepancies suggest device tampering or two
devices sharing the same shared secret (a clone).

Probability of a clone escaping detection is $1:2^i$ where I is the
number of elapsed `DK*_INTERVALS` since the clone was made. With
default `DK*_INTERVAL` values (specified above) the auth server has
50% probability of clone detection within 2hrs. with 99.99% detection
within 14hrs.

Pseudocode:

~~~ c
uint8_t dk[4];
...
bool is_valid_dk(uint8_t *last_dk, uint32_t last_clock,
	             uint8_t *new_dk, uint32_t new_clock) {
  assert(last_clock < new_clock);
  uint8_t mask[4] = {0xff, 0xff, 0xff, 0xff};
  while (++last_clock < new_clock) {
	if (last_clock % DK0_INTERVAL == 0) {
	  /* Evolve last_dk[3] using algo above */
	  ...
	  mask[3] = mask[3] << 1
	};
    if (last_clock % DK1_INTERVAL == 0) {
	 /* Evolve last_dk[0]--last_dk[2] using algo above */
	...
	if (mask[2])
	  mask[2] = mask[2] << 1
	else if (mask[1])
	  mask[1] = mask[1] << 1
	else if (mask[0])
	  mask[0] = mask[0] << 1
	else
		return true;
	}
  };
  for (int i; i < 4; i+=) {
	if (!((last_dk[i] & mask[i]) == new_dk))
		return false;
  }
  return true;
}
~~~

# Security Beacon Authentication Server Workflow Overview

0. Un-frame listener report according to HDLC.

1. Filter and split secure beacon packets into components in
  [Secure Beacon Format](#secure-report-format)

2a. If first time seeing *BEACON_ID*, derive *BEACON_KEY* from
    *MASTER_KEY* and *BEACON_ID*. Store initial *monotonic clock*,
    *dk* and cached *BEACON_KEY* in the datastore.

2b. Else, retrieve from the datastore by `beacon_id` the following fields:
    - Last seen *monotonic clock* (`last_clock`)
    - Last seen *dk* (`last_dk`)
    - Beacon Secret (`beacon_secret`)

3. Authenticate then decrypt payload

	~~~
    EAX(nonce, beacon_id, msg, tag, beacon_secret) ->
	      plaintext || fail(error)
    ~~~

4. Check incoming clock is $\ge$ `last_clock`. Only packets with clock $\ge$
   value stored in the datastore *shall* be accepted.

5. If necessary, evolve the `lask_dk` and check for discrepancies
   using a method similar to that described in
   [Detecting Clones with DK](#detecting-clones-with-dk)

6. Update the datastore `last_clock` and `last_dk`

7. Use *Listener ID* (from packet header), *filtered distance* and
   *flags* fields as needed for access control; either logging data or
   triggering actions.
