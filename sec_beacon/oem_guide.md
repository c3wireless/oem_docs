XS% OEM Guide; C3 Security Beacons
% Shawn Nock, Jaycon Systems
% v1.1; 27 Jul. 2016

# System Overview

## Components

### Beacons

C3 makes a line of beacons broadly compatible with iBeacon™. Some
optional enhancements to iBeacon™ protocol are supported:

 - Battery level advertisement

    At programmable intervals, the beacon can transmit the current
    battery life percentage.

 - Longer Intervals

    For some applications the 100ms max advertisement interval
	specified by iBeacon isn't ideal. C3 Beacons can be programmed to
    advertise at intervals between 20ms and 10000ms so that the OEM may
    choose between increase battery life and increased location
    accuracy.

### Listener Hardware

Listeners are custom hardware featuring the following:

 - AR9331 clocked at 400MHz; Embedded Linux OS
 - 802.11bg Wireless Radio
 - 100Mbit/s Ethernet, including 802.3af Power-over-ethernet
 - 2.4GHz Bluetooth 4.0 Radio

Listeners monitor the BLE advertisement channels for iBeacon
compatible packets; beacon distance/power is filtered and aggregated
by the listener before being forwarded to the location service at
configurable intervals

The listener packet format is documented in the *packet-format*
document.

# Getting Started

## Listener Setup

### Listener Power

Listeners may be powered via 802.3af PoE (Power-over-ethernet) via the
wired ethernet connector *or* via a 5V power supply (center positive)
at the barrel jack.

### Connecting to Listeners
Windows prerequisites: [WinSCP](http://www.winscp.net) & [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/)

The listeners are configured via their wired networking port. This
port maintains the static address `192.168.7.1` (even when the port is
assigned another address for normal operation *or* DHCP).

The CLI may be accessed as follows

Linux / Mac:
:    `ssh root@192.168.7.1`

Windows:
:    Start->Run `cmd`;

	 `C:\Program Files\PuTTY\putty.exe -ssh root@192.168.7.1`

The default password is: `c3letmein`

![](cli.jpg)\

### Firmware Upgrade

1. Connect listener to computer via wired port.
2. Statically assign IP address to computer

	Linux:
	:    `ip addr add 192.168.7.2/24 dev eth0`

	Windows:
	:    [Howto Link](http://www.howtogeek.com/howto/19249/how-to-assign-a-static-ip-address-in-xp-vista-or-windows-7/?PageSpeed=noscript)

	MacOS:
	:    [Howto Link](http://www.howtogeek.com/howto/22161/how-to-set-up-a-static-ip-in-mac-os-x/?PageSpeed=noscript)

3. Copy firmware to Listener

    Linux / Mac:
    :    `scp *jffs2-sysupgrade.bin root@192.168.7.1:/tmp`

    Windows:
    :    [Upload with WinSCP](https://winscp.net/eng/docs/task_upload) (remote directory should be /tmp)
4. Connect to Listener CLI (see above)

5. Start upgrade procedure, by typing the following at the prompt:

    `sysupgrade -n /tmp/*jffs2-sysupgrade.bin`

![](firmware-upgrade.jpg)\

The firmware will be flashed and the listener will reboot. Changes to
the network settings will be lost after the upgrade (static IP for
wired *or* custom wireless ssid/key). Contact *C3 Wireless* for
integration of custom settings into a personalized firmware image.

### Listener Settings

The characteristics of the listeners may be changed in a number of
ways by loging into the listener and editing the
`/etc/c3listener.conf` file:

`server` (string; IP address or hostname; default = ""):
: The hostname of the remote server

`port` (string; default = "9999"):
: The remote port number on the remote server

`path_loss` (double; default = 4) :
: The RSSI model used for determining beacon distance needs a path
loss component to summarize the RF characteristics of the local
environment. In clear, outdoor environments this is often below 2.0,
indoors in office environments it is sometimes over 4.0.

`user` (string; default = "nobody"):
: The listening daemon drops privileges after setting up bluetooth
sockets. The daemon will switch to the permissions of this
user.

`report_interval` (integer; milliseconds, default = 500):
: The listening daemon prepares a summary of beacon traffic and sends
it to the server at this interval . No report is sent if no traffic is
observed during the interval

`keep_alive` (integer; seconds, default = 30):
: The listener will transmit a keep alive packet to the server at this
interval so it may distinguish between listener failure
and simply no beacons near the listener.

`haab` (double, meters; default = 0.0):
: *Height above average beacon*

`antenna_correction` (integer, dBm; default = 0.0)
: Antenna gain correction. If a high gain antenna is used, signal
levels reported for beacons will be higher than the advertised
dBm@1m. This value is added to RSSI before distance is
calculated. High gain antennas will require a positive value,
electrically compromised antenna will require negative values.

### Listener Tuning (Optional)

#### Listener Positioning

The listener antenna has a null looking directly into the point
of the antenna and maximum gain between $\ang{40}$--$\ang{80}$ off
axis.

Avoid placing the listener such that most common expected beacon paths
will dwell directly underneath it.

The ideal height for maximum gain is:

\begin{tikzpicture}[scale=0.25]
    \tkzInit[xmax=5,ymax=5]
	\tkzDefPoint(0,5){C}
	\tkzDefPoint(0,0){B}
	\tkzDefPoint(5,0){A}
	\tkzDefPoint(5,5){E}
	\tkzDefShiftPoint[C](-10:9){A'}
	\tkzDefShiftPoint[C](-80:9){A''}
	\tkzLabelPoint[above](C){Listener}
	\tkzMarkRightAngle(C,B,A)
	\tkzDefMidPoint(B,C) \tkzGetPoint{M}
    \tkzInterLL(C,A')(B,A) \tkzGetPoint{D}
	\tkzDefMidPoint(C,D) \tkzGetPoint{M'}
	\tkzInterLL(C,A'')(M,M') \tkzGetPoint{D'}
    \tkzInterLL(C,A')(M,M') \tkzGetPoint{D''}
    \tkzDrawSegments(B,M M,C C,D M,M')
	%\tkzFillPolygon[fill=yellow](D',D'',C)
	\tkzDrawPoints(M)
	\tkzLabelSegment[left](B,M){$h_{avg.\ beacon}$}
	\tkzLabelSegment[left](C,M){$x$}
	\tkzLabelSegment[below](M,M'){coverage\ distance}

\end{tikzpicture}

$$\tan\ang{80} \approx 5.7 = \frac{\mbox{coverage distance}}x$$
$$x = \frac{\mbox{coverage distance}}{5.7}$$
$$\mbox{ideal height} = x + h_{\mbox{avg. beacon}} =
\frac{\mbox{coverage distance}}{5.7} + h_{\mbox{avg. beacon}}$$

For example a listener expected to cover a $\SI{8}{\meter}$ radius, the
ideal height assuming an mean expected beacon height of
$\SI{1.5}{\meter}$ above the floor is: $\frac{\SI{8}{\meter}}{5.7} +
\SI{1.5}{\m} \approx \SI{2.9}{\meter}$ above the floor.

What does it mean, *ideal height*. In this context, it means that the
largest and most uniform portion of the antenna gain is oriented to
receive signals from the largest portion of the wanted coverage area.

In general:

1. Mount the listener on the ceiling
2. Avoid placing the listener directly above expected position of beacons
3. If you have a choice of mounting heights, best location results can
   be found near the ideal height.
4. Larger areas can be covered by placing the listener higher, at the
   cost of widening null zone directly underneath the listener.

The equation for x above, represents ideal HAAB (Height above average
beacon). Listeners may be informed of their actual HAAB via this
listener config file. This value may need tuning in the listener
software configuration. See the section on [HAAB](#haab-height-above-average-beacon).

#### Antenna positioning

1. Positioning the antenna perpendicular to and level with the
expected plane of beacons. Most commonly this will be $\ang{90}$ to the floor.

	In general, this means that listeners mounted vertically on a wall
	have straight antennae ($\ang{0}$) and those mounted on ceilings
	/ furniture have fully-bent ($\ang{90}$) antennae.

2. Position the antenna pointing toward the plane of expected beacons.

	If the listener is on a wall, mount the unit antennae downward for
    higher installations, antennae upward for lower
    installations. Similarly, if the listener is mounted horizontally
    on top of tall furniture, the antenna should face downward
    (counter-intuitive).

#### HAAB (Height Above Average Beacon)

Positioning listeners above obstacles can improve reception / received
power of beacon transmissions, but will also increases the non-planar
distance the beacon signal must travel (more distance is traveled
vertically rather than horizontally).

Imagine that you are holding a beacon and standing directly underneath
a listener mounted 4 meters above you on the ceiling. The listener
would report that the beacon is 2-3 meters away, but if viewed on a
map, you'd rightfully expect the beacon should be geographically
co-located at the listener.

This can be corrected by use of the
[Pythagorean theorem](https://en.wikipedia.org/wiki/Pythagorean_theorem).

$$a^2 + b^2 = c^2$$

Where HAAB is one side of a right triangle and RSSI derived distance
is assumed to be the hypotenuse path of the signal; the planar
distance can be calculated

For the above example;
	let $a = \SI{3}{\meter}$ (HAAB),
		$b = b$ (unknown planar distance to beacon, in meters), and
		$c = \SI{3}{\meter}$ (signal path distance)

$$3^2 + b^2 = 3^2$$
$$b^2 = 0$$
$$b = \sqrt0 = \SI{0}{\meter}$$

If we move away from the listener and the signal path distance is
reported as 6m; the HAAB correction reduces the planar distance to:

$$3^2 + b^2 = 6^2$$
$$b = \sqrt{27} \approx\SI{5.2}{\meter}$$

In general, other error sources (multipath propagation, dynamic path
loss, lobes in antenna pattern) dominate the total system
error. However, in some environments with high ceilings or small rooms
where beacons are expected to be mostly underneath the listener tuning
this value may be desired.

#### Antenna Gain

Listeners are generally shipped with a dipole with RP-SMA
connector. This unit/antenna combination is the one again which
reference beacon signal strength is calculated.

If a different antenna is substituted, it will be necessary to modify
the listener `antenna_correction`. High-gain / directional antennas
will require a negative value, electrically compromised antenna will
require positive values in order to produce accurate location data.

The units of `antenna_correction` are dBm. For calibration of unknown
antenna; a beacon may be placed a known distance the listener /
antenna and the `antenna_correction` may be adjusted until the
reported distance is equal to the known distance. Increasing values of
`antenna_correction` result in a decrease in reported distance,
reducing `antenna_correction` results in a larger reported distance.

### Listener Networking

By default, listeners use DHCP on their wired Ethernet connection for
the connection details required to reach the location server.

#### Static IP

In order to set a static address for the listener, you'll need to
connect to the listener and edit the `/etc/config/network` file.

1. Connect to listener CLI (as above)
2. `nano /etc/config/network`

At minimum, the `lan2` interface should be changed to look similar to
the `lan` interface. You'll also need to add `option gateway
<gw_ipaddr>`.

![](static-ip.jpg)\

3. Save changes by typing `CTRL-X`.
4. type: `sync; reboot`

Further configuration documentation is available in the
[OpenWRT network documentation](https://wiki.openwrt.org/doc/uci/network)

### Wireless Network

Wireless network access is configured in the `/etc/config/wireless`

1. Connect to CLI (as above)
2. `nano /etc/config/wireless`

The fields of interest are `option ssid`, `option encryption` and
`option key`. By default, connection is attempted to the wireless and
an address is requested via DHCP.

![](wireless-config.jpg)\

3. `CTRL-X` to save
4. type: `sync; wifi`
5. Try pinging the location server: `ping core4.nocko.se`
6. If the ping is successful (*$x$ bytes received*), then type `reboot`

Further configuration documentation is available in the
[OpenWRT wireless documentation](https://wiki.openwrt.org/doc/uci/wireless)

## Beacon Notes

### Beacon Orientation

The standard beacon has an on board omnidirectional antenna on the
right side (with beacon oriented with the C3 logo facing
you).

The emissions are polarized in the same plane as the antenna. For best
reception, you should consider mounting the beacon so that the battery
door is parallel to the floor.

![Beacon Antenna](beacon-antenna.jpg){height=0.75in}

# Security Beacon - Packet Formats

## Report Framing

Since the protocol is based on TCP, framing is required to detect the
start and stop of each report. The framing used is the *Asynchronous Framing* from the *High-Level Data Link Control (HDLC)* [^hdlc]

[^hdlc]: [https://en.wikipedia.org/wiki/High-Level_Data_Link_Control#Asynchronous_framing](https://en.wikipedia.org/wiki/High-Level_Data_Link_Control#Asynchronous_framing)

The basic method is using the octet 0x7e as the frame boundary. Each
report will start and end with 0x7e. If 0x7e is present as data inside
the report it is escaped with the octet 0x7d and the fifth bit is
cleared. If the escape character is present in the data it's escaped
with itself and it's fifth bit is cleared. In practice this can be
accomplished simply with a translation table.

Translation Table for framing:

| Data Byte | |Value in frame |
|-----------|-|---------------|
| 0x7e      | → |{0x7d, 0x5e}  |
| 0x7d      | →| {0x7d, 0x5d}  |
| Other     | →| No change     |

Translation Table for un-framing (as done at the authserver):

| Value in frame | |Data Byte |
|-----------|-|---------------|
| {0x7d, 0x5e}      | → | 0x7e |
|{0x7d, 0x5d}      | →|  0x7d  |
| Other     | →| No change     |


The report format that follows assumes that the leading and trailing
0x7e octect has been stripped and any escaped values in between have
been *unescaped* back to their original values.

## Header Format

````
|---|-----------------|---|
             Data ------^
         ^-- Listener Name (Wifi MAC Addr)
  ^--------- Header (3 bytes)
````

### Header Byte 0x00: Protocol Version & Packet Type

The most significant 4 bytes of byte 1 identify the version of the
protocol that the listener speaks.

	 version = (uint8_t) byte[0] >> 4;
	 packet_type = (uint8_t)(byte[0] & 0x0f);

Current Versions:

 * 0x00: Initial Version

The least significant four bytes identify the packet type.

Current Packet Types:

 * 0x00: Keepalive Packet
 * 0x01: Data Packet
 * 0x02: Security Beacon Packet

See Packet Types section for details.

### Header Byte 0x01: Report/data unit size

Security Beacon Packets contain only one report in the payload, this
field therefore is the size of the secure report data in bytes.

    report_len = byte[1]

For a Keepalive packet this byte may be set to any value, it *shall*
be ignored by the auth server pending future use.

### Header Byte 0x02: Listener identifier length

The length of the listener identifyer that follows.

    listener_id_len = byte[2]

Generally, listeners are identified by the mac address of their
onboard WiFi NIC and this identifier is 6 bytes. However, the
identifier may be any series of bytes $<= 255$ bytes.

### Byte 0x03--`(0x03+listener_id_len-1)`: Listener Identifier

The following `listener_id_len` bytes are the listener_id.

### Byte `(0x03+listener_id_len)`--`EOF`: Payload

The remaining bytes in the packet is the payload in one of the
formats described below. The type of payload is 4 least-significant
bits of *Header Byte 0x00*

## Keepalive Packet

````
|---|-----------------|---|
                        ^-- Data (optional, ignored)
            ^-------------- Listener Name (Wifi MAC Addr)
  ^------------------------ Header (3 bytes)
````

Keepalive packets are sent by the listener if a data report hasn't
been sent for `KEEP_ALIVE_SEC` (c3listener.h:24) . It's intended to
disambiguate the no beacon activity and a malfuctioning/missing
listener conditions.

## Secure Beacon Packet

````
|---|-----------------|-----|
         Secure Report --^
       ^ Listener Name (Wifi MAC Addr)
  ^----- Header (3 bytes)
````

Secure beacon packets are generated for every recieved secure beacon
signal received and sent immediately to the server. The length of the
report is given in the header in byte 0x01.

### Secure Report Format

Within the report the data is structured as follows:

````
|----|----------------|---------|-------|-------|---|--|
   ^---------------- Beacon ID / MAC Addr (uint8_t[6])
             ^------ Nonce (uint8_t[16])
  Enciphered Payload (uint8_t[9]) --^
  MAC Tag (uint8_t[4]) --------------------^
  Filtered Distance (uint16_t, in cm) ------------^
  Distance Variance (m^2*100) -----------------------^
````

All multi-byte values (uint16_t and uint32_t) values are in network
byte order (big endian).

## Beacon Key Derivation

Each beacon is programmed with a 128bit secret *BEACON_KEY* that is
derived from a *MASTER_SECRET* specific to the organization and the
*BEACON_ID* (MAC Address). The Key derivation function (KDF) is
non-reversible and the *MASTER_SECRET* is not stored on the beacon;
thus the compromise of one beacon secret doesn't put other beacons at
risk. The key derivation function is not reversible.

When authenticating beacons for the first time, one must derive the
unique *BEACON_KEY*. The key is derived as follows:
CMAC-AES128(MASTER_KEY, *BEACON_ID*). This means the *BEACON_KEY* is
the result of a CMAC-AES128 round using the organization specific
*MASTER_KEY* as the key and *BEACON_ID* as the message.

## Deciphering and Authenticating the Payload

Once the relevant key is derived or a cache obtained via a datastore
the payload maybe decrypted and authenticated using the EAX mode of the
AES128 block cipher.

EAX mode is implemented in may cryptography libraries, an example and
some test vectors are given in the appendices. The information you'll
need to provide to the EAX implementations are:

- Shared Secret (16 bytes)
- Associated Data (Beacon ID)
- Message (Enciphered Payload)
- Tag (Message Authentication Code [MAC] Tag)

The library will check the *MAC* (which covers the encrypted message
and the Beacon ID). If the *message* is authentic, it will decrypt and
return the plaintext of the *message*

The EAX library used on the beacons is available for reference at
[on GitHub](https://github.com/nocko/nrf51_cryptolib)

## Plaintext Message Contents

Once the message is deciphered, the contents of the nine (9) byte
payload are:

Byte 0x00-0x03
:	Drifting Key (`uint8_t[4]`). See [Drifting Key](#drifting-key)

Byte 0x04-0x07
: Monotonic Clock (`uint32_t`). Advances once per second from initial
power on and is backed up to flash memory on the beacon at intervals.

Byte 0x08
:    Button Flags (`uint8_t`). Button 1 is lsb $\rightarrow$ Button 8 is
msb. (1 = button pressed during since last transmision, 0 = Not
pressed)

## Security Features

### Message Authentication Code

The packet (including *Beacon ID* as associated data) is protected by
a MAC tag via the EAX mode of AES128. It is computationally infeasible
to generate this tag without access to the shared secret.

### Replay Prevention

The encrypted *monotonic clock* field acts as a counter. The auth
server should track the most recent authenticated monotonic clock in
the datastore and reject any packet with a clock less than or equal to
the last known good clock.

A secure beacon flushes this clock to flash memory at longer intervals
to save power. If a beacon is restarted for some reason (battery
change); the packets it generates for the subsequent interval will be
replays until the monotonic clock advances past the last seen at the
auth server.

### Drifting Key

To detect cloned beacons, the payload contains a *drifting key*. The
beacon updates the key periodically with random information. The
expected uncertainty in the key is a function of the difference
between the last seen *monotonic clock* and the clock in the packet
being verified.

The auth server can use *inconsistant* uncertainty to discern that a
beacon has been tampered with or that two devices with the same shared
secret are operating (cloned device).

The datastore will have no initial reference for the drifting key. It
should be *trusted on first use*.

### Drifting Key Evolution

There are two update intervals `DK0_INTERVAL` (7200s) and
`DK1_Interval` (86400s).

When `monotonic_clock` % `DK0_INTERVAL` == 0:

The dk[3] (the last byte) is shifted left by one bit (but *not*
carried into dk[2]) and a random bit is added on the
right. Pseudocode:

~~~ c
uint8_t dk[4];
...
if (clock % DK0_INTERVAL == 0) {
	dk[3] = (dk[3] << 1) | rnd_bit();
}
~~~

When `monotonic_clock` % `DK1_INTERVAL` $===$ 0:

dk[0]--dk[2] are shifted as a group to the left by one bit and a
random bit is appended on the right. Pseudocode:

~~~ c
uint8_t dk[4];
...
if (clock % DK1_INTERVAL == 0) {
	dk[0] = (dk[0] << 1) | (dk[1] >> 7);
    dk[1] = (dk[1] << 1) | (dk[2] >> 7);
	dk[2] = (dk[2] << 1) | rnd_bit();
}
~~~

## Detecting clones with DK

The auth server *shall* evolve the stored last known good dk
(`last_dk`) forward to approximate the state of the incoming dk
(`new_dk`) by rotating in zeros according to the evolution
algorithm.

Additionally, a mask for the expected uncertainty in the incoming dk
is generated (see below). The masked incoming dk is compared to the
locally evolved dk. Discrepancies suggest device tampering or two
devices sharing the same shared secret (a clone).

Probability of a clone escaping detection is $1:2^i$ where I is the
number of elapsed `DK*_INTERVALS` since the clone was made. With
default `DK*_INTERVAL` values (specified above) the auth server has
50% probability of clone detection within 2hrs. with 99.99% detection
within 14hrs.

Pseudocode:

~~~ c
uint8_t dk[4];
...
bool is_valid_dk(uint8_t *last_dk, uint32_t last_clock,
	             uint8_t *new_dk, uint32_t new_clock) {
  assert(last_clock < new_clock);
  uint8_t mask[4] = {0xff, 0xff, 0xff, 0xff};
  while (++last_clock < new_clock) {
	if (last_clock % DK0_INTERVAL == 0) {
	  /* Evolve last_dk[3] using algo above */
	  ...
	  mask[3] = mask[3] << 1
	};
    if (last_clock % DK1_INTERVAL == 0) {
	 /* Evolve last_dk[0]--last_dk[2] using algo above */
	...
	if (mask[2])
	  mask[2] = mask[2] << 1
	else if (mask[1])
	  mask[1] = mask[1] << 1
	else if (mask[0])
	  mask[0] = mask[0] << 1
	else
		return true;
	}
  };
  for (int i; i < 4; i+=) {
	if (!((last_dk[i] & mask[i]) == new_dk))
		return false;
  }
  return true;
}
~~~

# Authentication Server Workflow Overview

0. Un-frame listener report according to HDLC.

1. Filter and split secure beacon packets into components in
  [Secure Beacon Format](#secure-report-format)

2a. If first time seeing *BEACON_ID*, derive *BEACON_KEY* from
    *MASTER_KEY* and *BEACON_ID*. Store initial *monotonic clock*,
    *dk* and cached *BEACON_KEY* in the datastore.

2b. Else, retrieve from the datastore by `beacon_id` the following fields:
    - Last seen *monotonic clock* (`last_clock`)
    - Last seen *dk* (`last_dk`)
    - Beacon Secret (`beacon_secret`)

3. Authenticate then decrypt payload

	~~~
    EAX(nonce, beacon_id, msg, tag, beacon_secret) ->
	      plaintext || fail(error)
    ~~~

4. Check incoming clock is $\ge$ `last_clock`. Only packets with clock $\ge$
   value stored in the datastore *shall* be accepted.

5. If necessary, evolve the `lask_dk` and check for discrepancies
   using a method similar to that described in
   [Detecting Clones with DK](#detecting-clones-with-dk)

6. Update the datastore `last_clock` and `last_dk`

7. Use *Listener ID* (from packet header), *filtered distance* and
   *flags* fields as needed for access control; either logging data or
   triggering actions.
